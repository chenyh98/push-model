package org.jlu.dede.pushModel.util;

import java.util.List;
import java.util.Map;

public interface JPushUtil {
    /**
     * 推送通知：以广播形式推送给所有平台
     * @return
     */
    public static void buildPushObject_all_alias_alert(){}
    /**
     * 推送通知：根据alias推送给个人
     * @param alias
     * @param notification_title
     * @param msg_title
     * @return
     */
    public static void buildPushObject_all_alias_alertWithTitle(String alias,String notification_title, String msg_title){}
    /**
     * 推送通知和自定义消息：根据alias推给多个用户
     * @param aliases
     * @param notification_title
     * @param msg_title
     * @return
     */
    public static void buildPushObject_all_aliases_alertAndmessage(List<String> aliases,String notification_title,String msg_title){}
    /**
     * 推送通知:根据alias同时推送给多个用户
     * @param aliases
     * @param notification_title
     * @param msg_title
     * @param extrasparam
     * @return
     */
    public static void buildPushObject_all_aliases_alertWithTitle(List<String> aliases, String notification_title, String msg_title, Map<String,String> extrasparam){}
    /**
     * 推送自定义消息:根据alias同时推送给多个用户
     * @param aliases
     * @param msg_content
     * @param extrasparam
     * @return
     */
    public static void buildPushObject_all_aliases_message(List<String> aliases,String msg_content,Map<String, String> extrasparam){}
    /**
     * 推送通知和自定义消息：根据alias推给个人
     * @param alias
     * @param msg_content
     * @param extrasparam
     * @return
     */
    public static void buildPushObject_all_alias_alertAndmessage(String alias,String msg_content,Map<String, String> extrasparam ){}
    /**
     * 推送自定义消息：根据alias推给个人
     * @param alias
     * @param msg_title
     * @param msg_content
     * @param extrasparam
     * @return
     */
    public static void buildPushObject_all_alias_message(String alias,String msg_title,String msg_content,Map<String, String> extrasparam ){}
    /**
     * 推送通知：根据tag推送给个人
     * @param tag
     * @param tag_and
     * @param notification_title
     * @param msg_title
     * @param extrasparam
     * @return
     */
    @SuppressWarnings("static-access")
    public static void buildPushObject_all_tag_alertWithTitle(String tag,String tag_and,String notification_title, String msg_title, Map<String,String> extrasparam){}
    /**
     * 推送通知和自定义消息：根据tag推给个人
     * @param tag
     * @param tag_and
     * @param notification_title
     * @param msg_title
     * @param msg_content
     * @param extrasparam
     * @return
     */
    @SuppressWarnings("static-access")
    public static void buildPushObject_all_tag_alertAndmessage(String tag,String tag_and,String notification_title,String msg_title,String msg_content,Map<String, String> extrasparam ){}
    /**
     * 推送自定义消息：根据tag推给个人
     * @param tag
     * @param msg_title
     * @param msg_content
     * @param extrasparam
     * @return
     */
    public static void buildPushObject_all_tags_message(String[] tag,String msg_title,String msg_content,Map<String, String> extrasparam ){}



}

