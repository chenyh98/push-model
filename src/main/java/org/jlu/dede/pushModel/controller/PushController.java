package org.jlu.dede.pushModel.controller;

import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Push;
import org.jlu.dede.pushModel.service.PushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PushController {
    @Autowired
    PushService pushService;

    //推送分为通知和自定义消息
        //通知就是手机的通知栏提醒
        //自定义消息是应用内收到处理后选择展示给用户的
    //alias是应用的识别，叫别名，命名规则是乘客："p"+id，司机：："d"+id，id是类中的id属性
        //通过alias就可以找到乘客端和司机端
        //推送给多个用户，要用List<String>传所有alias
    //notificationTitle是通知的标题
    //notificationContent是通知的内容
    //msgContent是自定义消息的内容，可以为空
    //objType是要传的类的type，以便前端识别Json并处理，比如Passenger类就是Passenger，Double distance就是Double
    //obj是要传的类
    //objType和obj组成自定义消息的extra内容，是给前端的主要内容，前端接收后就可以用了


    @PostMapping("/pushAll")
    public boolean pushAll(@RequestBody Push apush)
    {
        return pushService.pushAll(apush.getMsgContent(), apush.getObjType(), apush.getObj());
    }

    //推送通知和自定义消息：根据alias推给个人
    @PostMapping("/pushOne/pushAlertAndMessage")
    public boolean pushOneAlertAndMessage(@RequestBody Push apush)
    {
        return pushService.pushAlertAndMessage(apush.getAlias(), apush.getNotificationTitle(), apush.getNotificationContent(), apush.getMsgContent(), apush.getObjType(), apush.getObj());
    }
    //推送通知：根据alias推给个人
    @PostMapping("/pushOne/pushAlert")
    public boolean pushOneAlert(@RequestBody Push apush)
    {
        return pushService.pushAlert(apush.getAlias(), apush.getNotificationTitle(), apush.getNotificationContent());
    }
    //推送自定义消息：根据alias推给个人
    @PostMapping("/pushOne/pushMessage")
    public boolean pushOneMessage(@RequestBody Push apush)
    {
        return pushService.pushMessage(apush.getAlias(), apush.getMsgContent(), apush.getObjType(), apush.getObj());
    }
    //推送通知和自定义消息：根据alias推给多个用户
    @PostMapping("/pushMany/pushAlertAndMessage")
    public boolean pushManyAlertAndMessage(@RequestBody Push apush)
    {
        return pushService.pushAlertAndMessage(apush.getAliases(), apush.getNotificationTitle(), apush.getNotificationContent(), apush.getMsgContent(), apush.getObjType(), apush.getObj());
    }
    //推送通知：根据alias推给多个用户
    @PostMapping("/pushMany/pushAlert")
    public boolean pushManyAlert(@RequestBody Push apush)
    {
        return pushService.pushAlert(apush.getAlias(), apush.getNotificationTitle(), apush.getNotificationContent());
    }
    //推送自定义消息：根据alias推给多个用户
    @PostMapping("/pushMany/pushMessage")
    public boolean pushManyMessage(@RequestBody Push apush)
    {
        return pushService.pushMessage(apush.getAliases(), apush.getMsgContent(), apush.getObjType(), apush.getObj());
    }

    @PostMapping("/driver/pullBookingOrder")
    public List<Order> pullBookingOrder(@RequestParam Integer id)
    {
        return pushService.pullBookingOrder(id);
    }

    @PostMapping("/driver/pullCurrentOrder")
    public List<Order> pullCurrentOrder(@RequestParam Integer id)
    {
        return pushService.pullCurrentOrder(id);
    }
    @PostMapping("/save/pullCurrentOrder/{id}")
    public boolean savePullCurrentOrder(@RequestBody List<String> driverList,@PathVariable("id") Integer id)
    {
        return pushService.savePullCurrentOrder(driverList,id);
    }
    @PostMapping("/save/pullBookingOrder/{id}")
    public boolean savePullBookingOrder(@RequestBody List<String> driverList,@PathVariable("id") Integer id){
        return pushService.savePullBookingOrder(driverList,id);
    }

}
