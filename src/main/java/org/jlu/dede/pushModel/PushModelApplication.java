package org.jlu.dede.pushModel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PushModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(PushModelApplication.class, args);
    }

}
