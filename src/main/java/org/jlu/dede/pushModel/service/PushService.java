package org.jlu.dede.pushModel.service;

import com.alibaba.fastjson.JSON;
import com.sun.org.apache.xpath.internal.objects.XObject;
import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.pushModel.util.JPushUtilImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.*;

@Service
public class PushService {

    @Autowired
    private StringRedisTemplate srt;

    /**
     * 类转化为Json
     *
     * @param obj
     * @return Map<String, String>
     */
    public Map<String, String> messageToMap(String objType, Object obj)
    {
        Map<String, String> extraspara = new HashMap<String, String>();
        String jsonString = JSON.toJSONString(obj);
        extraspara.put(objType, jsonString);
        return extraspara;
    }

    public boolean pushAll(String msgContent, String objType, Object obj)
    {
        JPushUtilImpl.buildPushObject_all_alias_alert(msgContent,messageToMap(objType, obj));
        return true;
    }

    /**
     * 推送通知和自定义消息：根据alias推给个人
     *
     * @param alias
     * @param notificationTitle
     * @param notificationContent
     * @param msgContent
     * @param objType
     * @param obj
     * @return
     */
    public boolean pushAlertAndMessage(String alias, String notificationTitle, String notificationContent, String msgContent, String objType, Object obj) {
        JPushUtilImpl.buildPushObject_all_alias_alertAndmessage(alias, notificationTitle, notificationContent, msgContent, messageToMap(objType, obj));
        return true;
    }

    /**
     * 推送通知：根据alias推给个人
     *
     * @param alias
     * @param notificationTitle
     * @param notificationContent
     * @return
     */
    public boolean pushAlert(String alias, String notificationTitle, String notificationContent) {
        JPushUtilImpl.buildPushObject_all_alias_alertWithTitle(alias, notificationTitle, notificationContent);
        return true;
    }

    /**
     * 推送自定义消息：根据alias推给个人
     *
     * @param alias
     * @param msgContent
     * @param objType
     * @param obj
     * @return
     */
    public boolean pushMessage(String alias, String msgContent, String objType, Object obj) {
        JPushUtilImpl.buildPushObject_all_alias_message(alias, msgContent, messageToMap(objType, obj));
        return true;
    }

    /**
     * 推送通知和自定义消息：根据alias推给多个用户
     *
     * @param alias
     * @param notificationTitle
     * @param notificationContent
     * @param msgContent
     * @param objType
     * @param obj
     * @return
     */
    public boolean pushAlertAndMessage(List<String> alias, String notificationTitle, String notificationContent, String msgContent, String objType, Object obj) {
        JPushUtilImpl.buildPushObject_all_aliases_alertAndmessage(alias, notificationTitle, notificationContent, msgContent, messageToMap(objType, obj));
        return true;
    }

    /**
     * 推送通知：根据alias推给多个用户
     *
     * @param alias
     * @param notificationTitle
     * @param notificationContent
     * @return
     */
    public boolean pushAlert(List<String> alias, String notificationTitle, String notificationContent) {
        JPushUtilImpl.buildPushObject_all_aliases_alertWithTitle(alias, notificationTitle, notificationContent);
        return true;
    }

    /**
     * 推送自定义消息：根据alias推给多个用户
     *
     * @param alias
     * @param msgContent
     * @param objType
     * @param obj
     * @return
     */
    public boolean pushMessage(List<String> alias, String msgContent, String objType, Object obj) {
        JPushUtilImpl.buildPushObject_all_aliases_message(alias, msgContent, messageToMap(objType, obj));
        return true;
    }
    @Autowired
    DriverFeign driverFeign;
    @Autowired
     OrderFeign orderFeign;
    long t = System.currentTimeMillis();//获得当前时间的毫秒数
    Random rd = new Random(t);//作为种子数传入到Random的构造器中
    public boolean savePullCurrentOrder(List<String> driverList,Integer id) {
        Iterator<String> iterator = driverList.iterator();
        System.out.println("begin while");
        Order order = orderFeign.getByID(id);
        while (iterator.hasNext()) {
            String sid = iterator.next();
            Integer iid = Integer.parseInt(sid.substring(1));
            System.out.println(iid);
            Driver driver = driverFeign.getByID(iid);
            if (driver != null) {
                if ((driver.getState() == 1 && order.getType() != 1) || (driver.getState() == 2 && order.getType() != 2))
                    iterator.remove();
                int rand = rd.nextInt(100);
                srt.opsForValue().set("CurrentOrder" + iid.toString() + rand, id.toString());
                System.out.println(srt.opsForValue().get("CurrentOrder" + iid.toString() + rand));
            }
        }
        return true;
    }
    public boolean savePullBookingOrder(List<String> driverList,Integer id) {
        Iterator<String> iterator = driverList.iterator();
        System.out.println("begin while");
        Order order = orderFeign.getByID(id);
        while (iterator.hasNext()) {
            String sid = iterator.next();
            Integer iid = Integer.parseInt(sid.substring(1));
            System.out.println(iid);
            Driver driver = driverFeign.getByID(iid);
            if (driver != null) {
                if ((driver.getState() == 1 && order.getType() != 1) || (driver.getState() == 2 && order.getType() != 2))
                    iterator.remove();
                int rand = rd.nextInt(100);
                srt.opsForValue().set("BookingOrder" + iid.toString() + rand, id.toString());
                System.out.println(srt.opsForValue().get("BookingOrder" + iid.toString() + rand));
            }
        }
        return true;
    }
    // 拉去订单：拉取预约订单给指定driver

    public List<Order> pullCurrentOrder(Integer id) {
        List<Order> obj = new ArrayList<Order>();
        Set<String> keys=srt.keys("CurrentOrder"+id.toString()+"*");
        Order order;
        order = null;
        for(String key:keys)
        {   System.out.println(key);
            order = orderFeign.getByID(Integer.parseInt(srt.opsForValue().get(key)));
            if (order.getState()==0&&order!=null)
                obj.add(order);
        }
        return obj;

    }


    // 拉去订单：拉取预约订单给指定driver

    public List<Order> pullBookingOrder(Integer id) {
        List<Order> obj = new ArrayList<Order>();
        Set<String> keys=srt.keys("BookingOrder"+id.toString()+"*");
        Order order;
        for(String key:keys)
        {
            System.out.println(key);
            order = orderFeign.getByID(Integer.parseInt(srt.opsForValue().get(key)));
            if (order.getState()==0)
                obj.add(order);
        }
        return obj;
    }

}

