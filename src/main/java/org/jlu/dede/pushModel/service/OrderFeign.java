package org.jlu.dede.pushModel.service;

import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface OrderFeign {

    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    Integer addOrder(@RequestBody Order order);

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.POST)
    void updateOrder(@PathVariable("id") Integer id, @RequestBody Order order);

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET)
    Order getByID(@PathVariable("id") Integer id);

    @RequestMapping(value = "/orders/passenger/{id}", method = RequestMethod.GET)
    List<Order> getByPassenger(@PathVariable Integer id);

    @RequestMapping(value = "/orders/driver/{id}")
    List<Order> getByDriver(@PathVariable("id") Integer id);

}
