package org.jlu.dede.pushModel.service;

import org.jlu.dede.publicUtlis.model.Account;
import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "data-access",url="10.100.0.2:8765")
public interface DriverFeign {

    @RequestMapping(value = "/drivers/{id}", method = RequestMethod.POST)
    void updateDriver(@PathVariable("id") Integer id, @RequestBody Driver driver);

    @RequestMapping(value = "/drivers/{id}", method = RequestMethod.GET)
    Driver getByID(@PathVariable("id") Integer id);

    @RequestMapping(value = "/drivers/{id}/order",method = RequestMethod.POST)
    boolean driverOrder(@PathVariable Integer id, @RequestBody Order order);

    @RequestMapping("/drivers/order/{id}")
    Driver findByOrder(@PathVariable Integer id);

    @GetMapping("/driver/{id}")
    Account getByDriver(@PathVariable Integer id);

    @GetMapping("/orders/driver/current/{id}")
    Order getCurrentByDriver(@PathVariable Integer id);

}
