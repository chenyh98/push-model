package org.jlu.dede.pushModel.demo;

import com.alibaba.fastjson.JSON;
import org.jlu.dede.publicUtlis.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PushDemo {
//    public static void main(String[] args) {
//        Integer id = 1;
//
//        Driver driver = new Driver();
//        driver.setId(1);
//        driver.setX("123");
//        driver.setY("456");
//        driver.setCertificate("xxxx-xxxx-xxxx-xxxx");
//        driver.setState(1);
//        driver.setOrder(null);
//
//        driver.setCurrentCar(new Car());
//        driver.getCurrentCar().setId(1);
//        driver.getCurrentCar().setNum("吉A·88888");
//        driver.getCurrentCar().setColor("white");
//        driver.getCurrentCar().setType("1");
//        driver.getCurrentCar().setCarLoad(4);
//        driver.getCurrentCar().setDriver(driver);
//
//        driver.setAccount(new Account());
//        driver.getAccount().setId(1);
//        driver.getAccount().setName("中国好司机");
//        driver.getAccount().setAge(3);
//        driver.getAccount().setEmail("123123123@qq.com");
//        driver.getAccount().setPassword("321321321");
//        driver.getAccount().setMoney(null);
//        driver.getAccount().setPhone("110");
//        driver.getAccount().setRegisterTime("9102:1:1");
//        driver.getAccount().setSex("男");
//        driver.getAccount().setState(1);
//        driver.getAccount().setType(1);
//        driver.getAccount().setVerifyValue(99);
//
//        Order order = new Order();
//        order.setId(1);
//        order.setPassenger(new Passenger());
//        order.getPassenger().setId(1);
//        order.setDriver(driver);
//
//        List<Integer> ids = new ArrayList<Integer>();
//        ids.add(1);
//        ids.add(2);
//
//        Map<String, String> XY = new HashMap<String, String>();
//        XY.put("123", "456");
//
//        double distance = 123.456;
//
//        PushService pushService = new PushService();
//        Object obj = driver;
//        String jsonString = JSON.toJSONString(obj);
//        System.out.println(jsonString);
//    }
}


//public class Demo {
//    public static void main(String[] args) {
////        String alias = "cyh";
////        //通知栏
////        String notification_title = "通知";
////        String msg_title = "有订单了请查看";
////        //自定义信息
////        String msg_content = "name:cyh,";
////        Map<String, String> extraspara = new HashMap<String, String>();
////        extraspara.put("key", "1");
////        extraspara.put("time", "11:53");
////        JPushUtilImpl.buildPushObject_all_alias_alertAndmessage(alias, notification_title, msg_title, msg_content, extraspara);
//
//        Integer id = 1;
//
//        Driver driver = new Driver();
//        driver.setId(1);
//        driver.setX("123");
//        driver.setY("456");
//        driver.setCertificate("xxxx-xxxx-xxxx-xxxx");
//        driver.setState(1);
//        driver.setOrder(null);
//
//        driver.setCurrentCar(new Car());
//        driver.getCurrentCar().setId(1);
//        driver.getCurrentCar().setNum("吉A·88888");
//        driver.getCurrentCar().setColor("white");
//        driver.getCurrentCar().setType("1");
//        driver.getCurrentCar().setCarLoad(4);
//        driver.getCurrentCar().setDriver(driver);
//
//        driver.setAccount(new Account());
//        driver.getAccount().setId(1);
//        driver.getAccount().setName("中国好司机");
//        driver.getAccount().setAge(3);
//        driver.getAccount().setEmail("123123123@qq.com");
//        driver.getAccount().setPassword("321321321");
//        driver.getAccount().setMoney(null);
//        driver.getAccount().setPhone("110");
//        driver.getAccount().setRegisterTime("9102:1:1");
//        driver.getAccount().setSex("男");
//        driver.getAccount().setState(1);
//        driver.getAccount().setType(1);
//        driver.getAccount().setVerifyValue(99);
//
//        Order order = new Order();
//        order.setId(1);
//        order.setPassenger(new Passenger());
//        order.getPassenger().setId(1);
//        order.setDriver(driver);
//
//        List<Integer> ids = new ArrayList<Integer>();
//        ids.add(1);
//        ids.add(2);
//
//        Map<String, String> XY = new HashMap<String, String>();
//        XY.put("123", "456");
//
//        double distance = 123.456;
//
//        PushService pushService = new PushService();
//
//        //1:推送通知和自定义消息：给乘客端推送司机已经接受当前订单
////        pushService.pushToPassengerAcceptCurrentOrder(id, driver);
//        //2推送通知和自定义消息：给乘客端推送司机已经接受预约订单
////        pushService.pushToPassengerAcceptBookingOrder(id, driver);
//        //3推送通知和自定义消息：给乘客端推送司机已经开始订单
////        pushService.pushToPassengerStartCurrentOrder(id, order);
//        //4推送通知和自定义消息：给乘客端推送司机结束旅途
////        pushService.pushToPassengerEndCurrentOrder(id, order);
//        //5推送通知和自定义消息：给司机端推送乘客当前订单
////        pushService.pushToDriverCurrentOrder(ids, order);
//        //6推送通知和自定义消息：给司机端推送乘客预约订单
////        pushService.pushToDriverBookingOrder(ids, order);
//        //7推送通知和自定义消息：给司机端推送开始预约订单
////        pushService.pushToDriverStartBookingOrder(id, order);
//        //8推送通知和自定义消息：给司机端推送乘客结束旅途
////        pushService.pushToDriverEndCurrentOrder(id, order);
//        //9推送通知和自定义消息：给乘客端和司机端推送当前订单被取消
////        pushService.pushToBothCancelCurrentOrder(order);
//        //10推送通知和自定义消息：给乘客端和司机端推送预约订单被取消
////        pushService.pushToBothCancelBookingOrder(order);
//        //11推送自定义消息：给乘客端推送周围司机位置
////        pushService.pushToPassengerNearDriver(id, XY);
//        //12推送自定义消息：给乘客端和司机端推送更新距离
////        pushService.pushToBothUpdateDistance(order, distance);
//    }
//}

